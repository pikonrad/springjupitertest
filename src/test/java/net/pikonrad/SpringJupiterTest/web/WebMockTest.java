package net.pikonrad.SpringJupiterTest.web;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import net.pikonrad.SpringJupiterTest.calculation.Calculator;
import net.pikonrad.SpringJupiterTest.dataAccess.DataReader;
import net.pikonrad.SpringJupiterTest.services.HiService;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(EndPoints.class)
//@SpringBootTest
//@AutoConfigureMockMvc
public class WebMockTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HiService service;

    @MockBean
    private DataReader dataReader;

    @MockBean
    private Calculator calc;

    @Test
    public void greetingShouldReturnMessageFromService() throws Exception {
        MockPrepearator.prepeareMockHiService(service);
        this.mockMvc.perform(get("/hi")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hello, Mock")));
    }
    @Test
    public void valueServiceShouldReadFromDatareader() throws Exception {
        MockPrepearator.prepeareMockDataReader(dataReader);
        this.mockMvc.perform(get("/vs")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("5")));
    }
    @Test
    public void valueServiceShouldReadFromDatareader2() throws Exception {
        MockPrepearator.prepeareMockDataReader(dataReader);
        this.mockMvc.perform(get("/vsx")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("9")));
    }
    @Test
    public void valueServiceShouldCalc() throws Exception {
        MockPrepearator.prepeareMockCalculator(calc);
        this.mockMvc.perform(get("/addfive")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("8")));
    }

}