package net.pikonrad.SpringJupiterTest.web;

import net.pikonrad.SpringJupiterTest.calculation.Calculator;
import net.pikonrad.SpringJupiterTest.dataAccess.DataReader;
import net.pikonrad.SpringJupiterTest.services.HiService;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class MockPrepearator {
    public static void prepeareMockHiService(HiService service) {
        when(service.sayHi()).thenReturn("Hello, Mock");
    }

    public static void prepeareMockDataReader(DataReader dataReader) {
        when(dataReader.readDB(anyInt())).thenReturn(9);
        when(dataReader.readDB(3)).thenReturn(7);
        when(dataReader.readDB(2)).thenReturn(5);
    }

    public static void prepeareMockCalculator(Calculator c){
        when(c.add(anyInt())).thenAnswer(i->(Integer) i.getArgument(0)+3);
    }
}
