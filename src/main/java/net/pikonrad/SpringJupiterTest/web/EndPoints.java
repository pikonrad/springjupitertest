package net.pikonrad.SpringJupiterTest.web;

import net.pikonrad.SpringJupiterTest.calculation.Calculator;
import net.pikonrad.SpringJupiterTest.dataAccess.DataReader;
import net.pikonrad.SpringJupiterTest.services.HiService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EndPoints {

    private final HiService hiService;
    private final DataReader dataReader;
    private final Calculator calculator;

    public EndPoints(HiService hiService, DataReader dataReader, Calculator calculator) {
        this.hiService = hiService;
        this.dataReader = dataReader;
        this.calculator = calculator;
    }

    @RequestMapping("/hello")
    public @ResponseBody String greeting() {
        return "Hello, World";
    }

    @GetMapping("/hi")
    public @ResponseBody  String hi(){
        return hiService.sayHi();
    }

    @GetMapping("/v")
    public @ResponseBody int getV(){
        return dataReader.readDB(2);
    }
    @GetMapping("/vs")
    public @ResponseBody String getVs(){
        return Integer.toString(dataReader.readDB(2));
    }
    @GetMapping("/vsx")
    public @ResponseBody String getVsx(){
        return Integer.toString(dataReader.readDB(5));
    }
    @GetMapping("/addfive")
    public @ResponseBody String addFive(){
        return Integer.toString(calculator.add(5));
    }
}
