package net.pikonrad.SpringJupiterTest.calculation;

import org.springframework.stereotype.Service;

@Service
public class Calculator {
    int mem=0;
    public int add(int x){
        mem+=x;
        return mem;
    }
}
