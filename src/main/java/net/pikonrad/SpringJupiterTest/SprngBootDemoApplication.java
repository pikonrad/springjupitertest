package net.pikonrad.SpringJupiterTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprngBootDemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(SprngBootDemoApplication.class, args);
	}

}
