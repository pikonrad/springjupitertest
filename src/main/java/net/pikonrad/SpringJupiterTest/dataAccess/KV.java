package net.pikonrad.SpringJupiterTest.dataAccess;

import lombok.Value;

@Value
public class KV {
    int k;
    int v;
}
