package net.pikonrad.SpringJupiterTest.dataAccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataReader {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int readDB(int k){
        return jdbcTemplate
                .queryForObject("SELECT v from t WHERE k=2", Integer.class);    }
}
