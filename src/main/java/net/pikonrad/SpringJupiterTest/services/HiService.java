package net.pikonrad.SpringJupiterTest.services;

import org.springframework.stereotype.Service;

@Service
public class HiService {
    public String sayHi(){ return "hi there"; }
}
